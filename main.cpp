/** Copyright &copy; 2018, rostislav.vel@gmail.com.
 * \brief  Поиск по маске.
 * \author Величко Ростислав
 * \date   24.04.2018
 */

//////////////////////////////////////////////////////////////////////////////////////////////////// 
// Программа принимает в качестве параметров командной строки: 
// 1) Имя текстового файла, в котором должен идти поиск (размер файла - до 1Гб). Расширение файла - любое. 
// 2) Маску для поиска, в кавычках. Максимальная длина маски 100 символов. 
// 
// Вывод программы должен быть в следующем формате: 
//   - На первой строке - количество найденных вхождений. 
//   - Далее информация о каждом вхождении, каждое на отдельной строке, в порядке номеров строк, через пробел: 
//     номер строки (начиная с 1), позиция в строке (начиная с 1), само найденное вхождение. 
// 
// Дополнения: 
// - В текстовом файле кодировка только 7-bit ASCII 
// - Поиск с учетом регистра 
// - Вхождение не может включать перевод строки. Маска не может содержать символа перевода строки 
// - Найденные вхождения не должны пересекаться между собой. Из пересекающихся берется только первое. 
// - Пробелы и разделители участвуют в поиске наравне с другими символами. 
// - Можно использовать STL, Boost 1.65, возможности С++11 и С++14. 
// - Многопоточность нужно использовать обязательно. Однопоточные решения засчитываться не будут. 
// - Дополнительным плюсом будет разделение работы между потоками равномерно вне зависимости от количества строк во входном файле. 
// 
// ПРИМЕР 
// Файл input.txt: 
// 1  I've paid my dues 
// 2  Time after time. 
// 3  I've done my sentence 
// 4  But committed no crime. 
// 5  And bad mistakes ‒ 
// 6  I've made a few. 
// 7  I've had my share of sand kicked in my face 
// 8  But I've come through. 
// 
// Запуск программы: mtfind input.txt "?ad" 
// Ожидаемый результат: 
// 3 
// 5 5 bad 
// 6 6 mad 
// 7 6 had 
//////////////////////////////////////////////////////////////////////////////////////////////////// 


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <string>
#include <memory>

#include "Log.hpp"
#include "MtFind.hpp"
#include "SignalDispatcher.hpp"


static const char DEFAULT_FILE_PATH[] = "./input.txt";
static const char DEFAULT_MASK[] = "?ad";


struct GlobalArgs {
    std::string input_file_path;  /// параметр -f
    std::string mask;             /// параметр -m
} __global_args;


static const char *__opt_string = "f:m:h?";
////////////////////////////////////////////////////////////////////////////////////////////////////


void HelpMessage() {
    std::cout << "  Use:\n\t#mtfind -f ./input.txt -m \"?ad\"\n"
              << "  Args:\n"
              << "\t[-f]\t Input file path. Default: \"" << DEFAULT_FILE_PATH << "\"\n"
              << "\t[-m]\t Find mask. Default: \"" << DEFAULT_MASK << "\"\n"
              << "__________________________________________________________________\n\n"
              << "\tMax file size is " << MtFind::MAX_INPUT_FILE_LEN << "\n"
              << "\tMax mask size is " << MtFind::MAX_MASK_LEN << "\n"
              << "__________________________________________________________________\n\n";
    exit(EXIT_FAILURE);
}
////////////////////////////////////////////////////////////////////////////////////////////////////


typedef utils::SignalDispatcher SignalDispatcher;


int main(int argc_, char **argv_) {
    LOG_TO_STDOUT;
    int opt = 0;

    /// Инициализация globalArgs до начала работы с ней.
    __global_args.input_file_path = DEFAULT_FILE_PATH;      
    __global_args.mask = DEFAULT_MASK;       

    /// Обработка входных опций.
    opt = getopt(argc_, argv_, __opt_string);
    while(opt not_eq -1) {
        switch(opt) {
            case 'f':
                __global_args.input_file_path = optarg;
                break;
            case 'm':
                __global_args.mask = optarg;
                break;

            case 'h':
            case '?':
                HelpMessage();
                break;

            default: break;
        }
        opt = getopt(argc_, argv_, __opt_string);
    }
    
    /// Выполнение процесса поиска.
    MtFind mtfind(__global_args.input_file_path, __global_args.mask);
    
    if (not mtfind) {
        HelpMessage();
    } else {
        mtfind.start();
        SignalDispatcher(
            std::bind(&MtFind::stop, &mtfind), 
            [&] {
                LOG(DEBUG) << mtfind.result();
            });
    }

    return EXIT_SUCCESS;
}
