/** Copyright &copy; 2018, rostislav.vel@gmail.com.
 * \brief  Класс поиск подстроки.
 * \author Величко Ростислав
 * \date   24.04.2018
 */


#include <vector>
#include <string>
#include <atomic>
#include <thread>
#include <fstream>
#include <memory>
#include <map>


typedef std::unique_ptr<std::thread> PThread;
typedef std::vector<char> Buf;


struct Entry {
    size_t line;
    size_t start_pos;
    size_t pos;
    std::string str;
};
typedef std::vector<Entry> EntriesBuf;
typedef std::map<size_t, Entry> EntriesMap;


struct ReadInfo {
    size_t line; 
    size_t readed_size;
    size_t block_size;
    size_t file_size;
    Buf old_buf;
};


class MtFind { 
    class BlockChecker {
        size_t _id;
        Buf _buf;
        std::string _mask;
        EntriesBuf _entries;
        
        bool checkSymbol(const char m_, const char s_);
        
    public:
        BlockChecker(
            size_t id_, size_t line_, size_t pos_, 
            const Buf &buf_, const std::string &mask_);
      
        EntriesBuf& getEntries();
    };

    class Task {
        size_t _id;
        std::atomic_bool _is_run;
        PThread _thread;
        std::unique_ptr<BlockChecker> _bc;
        
        void reset();
        
    public: 
        Task(
            size_t id_, size_t line_, size_t pos_, 
            const Buf &buf_, const std::string &mask_);
        virtual ~Task();
        
        bool isRun();
        EntriesBuf& getEntries();
    };

    typedef std::unique_ptr<Task> PTask;
    typedef std::vector<PTask> FindTasks;
    
public:
    static const size_t MAX_INPUT_FILE_LEN = 1000000000;
    static const size_t MAX_MASK_LEN = 100;
    static const char ANY_SYMBOL = '?';

private:
    bool _is_error;
    std::atomic_bool _is_run;
    PThread _run_task;
    FindTasks _find_tasks;
    std::ifstream _file;
    ReadInfo _read_info;
    std::string _mask;
    std::string _result;

    size_t getFileSize(const std::string &file_);
    bool checkMask(const std::string &mask_);
    size_t getBlockSize();

    void run();
    Buf readBlock();
    void createTask(size_t id_);
    void combineResult(const EntriesMap &entries_);

public:
    explicit MtFind(const std::string &file_, const std::string &mask_);

    operator bool ();

    void start();
    void stop();
    std::string result();
};
