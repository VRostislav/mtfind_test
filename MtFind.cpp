#include <sys/stat.h>

#include <iostream>
#include <iterator>
#include <string>
#include <algorithm>
#include <sstream>

#include "Log.hpp"
#include "MtFind.hpp"


bool MtFind::BlockChecker::checkSymbol(const char m_, const char s_) {
    if (s_ == '\0') {
        return false; 
    } else if (m_ == MtFind::ANY_SYMBOL) {
        return true;
    }
    return (m_ == s_); 
}


MtFind::BlockChecker::BlockChecker(
    size_t id_, size_t line_, size_t pos_, const Buf &buf_, const std::string &mask_) 
    : _id(id_)
    , _buf(buf_)
    , _mask(mask_) 
{
    size_t ms = _mask.size();
    size_t bs = _buf.size();
    size_t b = 0;
    if (ms <= bs) {
        LOG(DEBUG) << _id << ": Start check: {" << std::string(buf_.begin(), buf_.end()) << "}";
        while ((ms + b) < bs) {
            size_t i = 0;
            std::string res;
            for ( ; i < ms; ++i) {
                char s = _buf[i + b];
                if (not checkSymbol(_mask[i], s)) {
                    break;
                }
                res.push_back(s);
            }
            if (i == ms) {
                size_t shift = b;
                if (pos_) {
                    shift -= ms;
                }
                _entries.push_back({line_, pos_, shift, res});
                LOG(INFO) << _id << ": {" << line_ << " " << pos_ << " " << shift <<  " " << res << "}";
            }
            ++b;
        }
    }
    if (_entries.empty()) {
        _entries.push_back({line_, pos_, b, ""});
        LOG(INFO) << _id << ": Empty block: {" << line_ << " " << pos_ << " " << b << "}";
    }
}


EntriesBuf& MtFind::BlockChecker::getEntries() {
    return _entries;
}
////////////////////////////////////////////////////////////////////////////////////////////////////


void MtFind::Task::reset() {
    if (_thread) {
        _thread->join();
        _thread.reset();
    }
}


MtFind::Task::Task(
    size_t id, size_t line_, size_t pos_, const Buf &buf_, const std::string &mask_) 
    : _id(id) 
{
    _is_run = true;
    _thread.reset(new std::thread([this, line_, pos_, buf_, mask_] {
        _bc.reset(new BlockChecker(_id, line_, pos_, buf_, mask_));
        _is_run = false;
    }));
}

MtFind::Task::~Task() {
    reset();
}


bool MtFind::Task::isRun() {
    return _is_run;
}


EntriesBuf& MtFind::Task::getEntries() {
    reset();
    return _bc->getEntries();
}
////////////////////////////////////////////////////////////////////////////////////////////////////


size_t MtFind::getFileSize(const std::string &file_) {
    struct stat buf;
    size_t fs = 0;
    if ((fs = stat(file_.c_str(), &buf)) not_eq 0) {
        LOG(ERROR) << "Stat failure error: " << fs;
    } else {
        fs = buf.st_size;
        LOG(DEBUG) << "File size is " << fs;
    }
    return fs;
}


bool MtFind::checkMask(const std::string &mask) {
    if (mask.size() <= MAX_MASK_LEN) {
        for (auto m : mask) {
            if (m == '\n') {
                LOG(ERROR) << "Mask containes return symbol.";
                return false;
            } 
        }
        return true;
    }
    LOG(ERROR) << "Mask is large.";
    return false;
}


size_t MtFind::getBlockSize() {
    size_t ts = _find_tasks.size();
    size_t bs = 0;
    do {
        bs = _read_info.file_size / ts;
        --ts;
    } while(bs < _mask.size());
    return bs;
}


void MtFind::run() {
    _is_run = true;
    size_t ts = _find_tasks.size();
    LOG(DEBUG) << "Tasks num is " << ts;
    EntriesMap entries;
    while (_is_run) {
        Buf old_buf;
        for (size_t id = 0; id < ts; ++id) { 
            if (_find_tasks[id] and not _find_tasks[id]->isRun()) {
                LOG(INFO) << "Accumulate blocks...";
                for (auto e : _find_tasks[id]->getEntries()) {
                    if (not e.str.empty()) {
                        entries.insert({e.start_pos, e});
                        LOG(INFO) 
                            << "insert {" 
                            << e.start_pos << " " << e.pos << " " << e.str 
                            << "}";
                    }
                }
                LOG(INFO) << "Blocks is accumulated ";
                _find_tasks[id].reset();
                createTask(id);
            } else if (not _find_tasks[id]) {
                createTask(id);
            }
        }
    }
    combineResult(entries);
    LOG(INFO) << result();
}


Buf MtFind::readBlock() {
    Buf buf;
    Buf &obuf = _read_info.old_buf;
    if (not obuf.empty()) {
        buf.insert(buf.begin(), obuf.begin(), obuf.end());
    }
    size_t i = 0;
    for ( ; i < _read_info.block_size; ++i) {
        size_t s = _file.get();
        if (_file.good()) {
            if (s == '\n') {
                ++_read_info.line;
                ++i;
                break;
            } else {
                buf.push_back(static_cast<char>(s));
            }
        } else {
            LOG(WARNING) << "Can`t get symbol from file.";
            i = 0;
            break;
        }
    }
    _read_info.readed_size += i;
    LOG(WARNING) << "Readed is " << i << ": " << _read_info.readed_size;
    return buf;
}


void MtFind::createTask(size_t id_) {    
    if (_read_info.readed_size < _read_info.file_size) {
        size_t line = _read_info.line;
        size_t rs = _read_info.readed_size;
        Buf buf = readBlock();
        size_t ms = _mask.size();
        if (ms < buf.size()) {
            LOG(DEBUG) << "Block " << (line + 1) << " \"" << std::string(buf.begin(), buf.end()) << "\"";
            Buf &obuf = _read_info.old_buf;
            obuf.clear();
            obuf.insert(obuf.begin(), buf.end() - ms, buf.end());
            _find_tasks[id_].reset(new Task(id_, line, rs, buf, _mask));
        }
    }
}


void MtFind::combineResult(const EntriesMap &entries) {
    std::stringstream ss;
    ss << "\nResult:\n" << entries.size() << "\n";
    for (auto e : entries) {
        if (not e.second.str.empty()) {
            ss << (e.second.line + 1) << " " << e.second.pos << " " << e.second.str << "\n";
        }
    }
    _result = ss.str();
}


MtFind::MtFind(const std::string &file_name_, const std::string &mask_) 
    : _is_error(false)
    , _is_run(false)
    , _find_tasks(std::thread::hardware_concurrency())
    , _read_info({0, 0, 0, 0})
    , _mask(mask_)
{
    if (((_read_info.file_size = getFileSize(file_name_)) <= MAX_INPUT_FILE_LEN) and 
        checkMask(mask_) and 
        (mask_.size() <= _read_info.file_size)) 
    {
        _file.open(file_name_.c_str(), std::ios::in | std::ios::binary);
        if (not _file.is_open()) {
            LOG(ERROR) << "File is not open.";
            _is_error = true;
        } else {
            _read_info.block_size = getBlockSize();
            LOG(DEBUG) << "Block size is " << _read_info.block_size;
        }
    }
}


MtFind::operator bool () {
    return (not _is_error);
}


void MtFind::start() {
    if (not _is_error) {
        LOG(ERROR) << "Start process...";
        _run_task.reset(new std::thread(std::bind(&MtFind::run, this)));
    }
}


void MtFind::stop() {
    if (_run_task) {
        LOG(ERROR) << "Stop request...";
        _is_run = false;
        _run_task->join();
        _run_task.reset();
        LOG(ERROR) << "Stoped.";
    }
}


std::string MtFind::result() {
    return _result;
}


